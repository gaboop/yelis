<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main/index');
});

Route::get('/cpanel', function () {
    return view('cpanel/index');
});
//PROYECTO
Route::get('cpanel/MisProyecto', function () {
    return view('cpanel/proyecto/MisProyecto');
});
Route::get('cpanel/NuevoProyecto', function () {
    return view('cpanel/proyecto/NuevoProyecto');
});
Route::get('cpanel/Participantes', function () {
    return view('cpanel/proyecto/Participantes');
});

//MICMAC
Route::get('cpanel/NuevoMicmac', function () {
    return view('cpanel/micmac/NuevoMicMac');
});
Route::get('/MisMicmac', function () {
    return view('cpanel/micmac/MisMicmac');
});
//FODA
Route::get('cpanel/NuevoFoda', function () {
    return view('cpanel/foda/NuevoFoda');
});
Route::get('/MisFoda', function () {
    return view('cpanel/foda/MisFoda');
});

